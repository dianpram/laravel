<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    br {
      margin-top: 5px;
      margin-bottom: 5px;
    }
  </style>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action="/welcome" id="form1" method="post">
    @csrf
    <label for="">First name :</label><br>
    <input type="text" name="nama_depan" id=""><br>
    <label for="">Last name :</label><br>
    <input type="text" name="nama_belakang" id=""><br>
    <label for="">Gender :</label><br>
    <input type="radio" id="male" name="fav_language" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="fav_language" value="Female">
    <label for="css">Female</label><br>
    <input type="radio" id="other" name="fav_language" value="Other">
    <label for="javascript">Other</label><br>
    <label for="">Nationality :</label><br>
    <select id="nationality">
      <option value="indonesia">Indonesia</option>
      <option value="italia">Italia</option>
    </select><br>
    <label for="">Language Spoken :</label><br>
    <input type="checkbox" id="bi" name="bi" value="Bahasa Indonesia">
    <label for="bi"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="english" name="english" value="English">
    <label for="vehicle2"> English</label><br>
    <input type="checkbox" id="other" name="other" value="Other">
    <label for="vehicle3"> Other</label><br>
    <label for="vehicle3"> Bio :</label><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
  </form>
</body>

</html>